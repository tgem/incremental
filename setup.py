from setuptools import find_packages
from setuptools import setup

setup(
    name='icmen',
    version='1.0',
    packages=find_packages(),
    url='',
    license='',
    author='Piotr Bielak, Maciej Falkiewcz, Tomasz Kajdanowicz, Kamil Tagowski',
    author_email='',
    description=''
)
