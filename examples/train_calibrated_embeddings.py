"""
Example of BasicICMEN usage
"""
import argparse
import math
import os

import networkx as nx
import numpy as np
from sklearn import linear_model
from sklearn.multioutput import MultiOutputRegressor

from icmen.embedding import model as emb_model
from icmen.embedding import scoring
from icmen.utils import dataset


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--walks',
                        help='List of graph embeddings',
                        nargs='+',
                        required=True)
    parser.add_argument('--snapshots',
                        help='List of graph embeddings',
                        nargs='+',
                        required=True)
    parser.add_argument('--reference-nodes-ratio',
                        help='Ratio of used reference nodes (example: 0.7)',
                        type=float,
                        required=True)
    parser.add_argument('--output-path',
                        help='Output path of embeddings',
                        required=True)
    parser.add_argument('--dimension',
                        help='Dimensions size of the embeddings',
                        required=True,
                        type=int)
    parser.add_argument('--workers',
                        help='Workers count for training embeddings',
                        type=int,
                        required=True)
    parser.add_argument('--skip-gram',
                        help='Training algorithm: 1 for skip-gram, 0 for CBOW',
                        type=int,
                        required=True)
    parser.add_argument('--min-count',
                        help='Ignores all nodes with total frequency lower than this',
                        type=int,
                        required=True)
    parser.add_argument('--ridge-regression-alpha',
                        help='Regularization strength of ridge '
                             'regression used for calibration '
                             'of the embeddings',
                        type=float,
                        required=False,
                        default=1.0)

    return parser.parse_args()


def main():
    # Get args
    args = get_args()
    w2v_params = {
        'size': args.dimension,
        'workers': args.workers,
        'min_count': args.min_count,
        'sg': args.skip_gram
    }

    # Load all data
    print('Loading data...')
    snapshots = [nx.read_gpickle(sg) for sg in args.snapshots]

    # Reference nodes count calculation
    min_number_of_nodes = np.min([graph.number_of_nodes() for graph in snapshots])
    reference_nodes_count = math.ceil(min_number_of_nodes * args.reference_nodes_ratio)

    # Training first  snapshot embeddings
    print('Calibrated embeddings training...')
    some_model = emb_model.RandomWalkEmbedding(w2v_params)
    embeddings = [
        some_model.train(
            rw_seqs=dataset.read_walks(args.walks[0])
        )
    ]

    # Training calibrated embeddings
    rm = MultiOutputRegressor(
        linear_model.Ridge(alpha=args.ridge_regression_alpha), n_jobs=args.workers
    )

    for emb_id in range(1, len(snapshots)):
        reference_nodes_scores = scoring.get_reference_nodes_scores(
            previous_graph=snapshots[emb_id - 1],
            current_graph=snapshots[emb_id]
        )

        reference_nodelist = scoring.get_reference_nodelist(
            scores_dict=reference_nodes_scores,
            reference_nodes_count=reference_nodes_count
        )

        emb = emb_model.CalibratedRandomWalkEmbedding(
            w2v_params, reference_nodelist=reference_nodelist
        )

        embeddings.append(
            emb.train(
                prev_emb=embeddings[emb_id - 1],
                regression_model=rm,
                rw_seqs=dataset.read_walks(args.walks[emb_id]),
            )
        )

    print('Saving models...')
    for emb_id, embedding in enumerate(embeddings):
        filename = f'F{emb_id}{emb_id + 1}'
        embedding.to_pickle(
            filepath=os.path.join(args.output_path, filename)
        )


if __name__ == '__main__':
    main()
