"""
Example of BasicICMEN usage
"""
import argparse

import networkx as nx

from icmen.embedding import model as emb_model
from icmen.fusion import validation as val
from icmen.fusion import incremental_model as inc_model


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--embeddings',
                        help='List of graph embeddings',
                        nargs='+',
                        required=True)
    parser.add_argument('--alpha',
                        help='Value of the alpha parameter [0,1]',
                        type=float,
                        required=True)
    parser.add_argument('--test-graph',
                        help='The graph to test the estimated embeddings on',
                        required=True)

    return parser.parse_args()


def main():
    # Get args
    args = get_args()

    # Load all data
    print('Loading data...')
    embeddings = [emb_model.Embedding.from_file(ef) for ef in args.embeddings]
    test_graph = nx.read_gpickle(args.test_graph)

    # Apply ICMEN model
    print('Running ICMEN...')
    icmen = inc_model.BasicICMEN(
        window_size=len(embeddings),
        alpha=args.alpha
    )
    pred_embedding = icmen.predict(embeddings)

    # Validate estimated embedding
    print('Testing estimated embedding...')
    test_val_fn = val.make_validation_fn(
        vm_cls=val.LinkPredictionValidationModel,
        graph_snapshot=test_graph
    )

    test_auc = (1.0 - test_val_fn(pred_embedding)) * 100.0

    # Print results
    print('Basic ICMEN parameters', icmen.parameters)
    print('Combined embedding AUC on test graph: %.2f %%' % test_auc)


if __name__ == '__main__':
    main()
