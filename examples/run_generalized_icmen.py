"""
Example of ICMEN usage
"""
import argparse

import networkx as nx
import numpy as np

from icmen.embedding import model as emb_model
from icmen.fusion import validation as val
from icmen.fusion import incremental_model as inc_model


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--embeddings',
                        help='List of graph embeddings',
                        nargs='+',
                        required=True)
    parser.add_argument('--last-snapshot',
                        help='Most recent graph snapshot',
                        required=True)
    parser.add_argument('--prior-strategy',
                        help='The prior that should be used in Dirichlet model',
                        choices=['uniform', 'increase'],
                        required=True)
    parser.add_argument('--test-graph',
                        help='The graph to test the estimated embeddings on',
                        required=True)

    return parser.parse_args()


def main():
    # Get args
    args = get_args()

    # Load all data
    print('Loading data...')
    embeddings = [emb_model.Embedding.from_file(ef) for ef in args.embeddings]
    last_snapshot = nx.read_gpickle(args.last_snapshot)
    prior_strategy = args.prior_strategy
    test_graph = nx.read_gpickle(args.test_graph)

    # Apply ICMEN model
    print('Running ICMEN...')
    icmen = inc_model.ICMEN(window_size=len(embeddings))
    icmen.fit(embeddings, last_snapshot, prior_strategy)
    pred_embedding = icmen.predict(embeddings)

    # Validate estimated embedding
    print('Testing estimated embedding...')
    test_val_fn = val.make_validation_fn(
        vm_cls=val.LinkPredictionValidationModel,
        graph_snapshot=test_graph
    )

    test_auc = (1.0 - test_val_fn(pred_embedding)) * 100.0

    # Print results
    print('Estimated ICMEN parameters:', np.round(icmen.parameters, 4))
    print('Combined embedding AUC on test graph: %.2f %%' % test_auc)


if __name__ == '__main__':
    main()
