"""
Dataset utils
"""
from collections import defaultdict

import numpy as np
import sklearn.utils as sk_utils


def split_dataset(edge_list, train_dev_split):
    items_count = len(edge_list)
    train_items_count = int(items_count * train_dev_split)

    train_dataset = edge_list[:train_items_count]
    dev_dataset = edge_list[train_items_count:]

    train_dataset = [(edge[0], edge[1]) for edge in train_dataset]
    dev_dataset = [(edge[0], edge[1]) for edge in dev_dataset]

    return train_dataset, dev_dataset


def prepare_dataset(pos_train, neg_train, pos_dev, neg_dev):
    y_pos_train = np.ones((len(pos_train), 1), dtype=int)
    y_neg_train = np.zeros((len(neg_train), 1), dtype=int)

    y_pos_dev = np.ones((len(pos_dev), 1), dtype=int)
    y_neg_dev = np.zeros((len(neg_dev), 1), dtype=int)

    x_train = np.concatenate((np.array(pos_train), np.array(neg_train)))
    y_train = np.concatenate((y_pos_train, y_neg_train))

    x_dev = np.concatenate((np.array(pos_dev), np.array(neg_dev)))
    y_dev = np.concatenate((y_pos_dev, y_neg_dev))

    x_train, y_train = sk_utils.shuffle(x_train, y_train, random_state=0)
    x_dev, y_dev = sk_utils.shuffle(x_dev, y_dev, random_state=0)

    return x_train, y_train, x_dev, y_dev


def create_link_prediction_dataset_from_graph(graph, split_proportion):
    edgelist = sort_edges_by_time(graph)

    pos_train, pos_dev = split_dataset(edgelist,
                                       split_proportion)

    neg_train = generate_negative_samples(graph, len(pos_train))
    neg_dev = generate_negative_samples(graph, len(pos_dev))

    x_train, y_train, x_dev, y_dev = prepare_dataset(pos_train, neg_train,
                                                     pos_dev, neg_dev)
    return x_train, y_train, x_dev, y_dev


def generate_negative_samples(graph, samples_count):
    neg_samples = []

    for _ in range(samples_count):
        init_node = np.random.choice(graph.nodes)
        second_node_list = list(graph.nodes())
        for edge in set(graph.edges(init_node)):
            second_node_list.remove(edge[1])

        second_node = np.random.choice(second_node_list)
        neg_samples.append((init_node, second_node))

    return neg_samples


def sort_edges_by_time(nx_graph):
    edge_list = list(nx_graph.edges(data=True))
    sorted_list = sorted(edge_list, key=lambda edge: edge[2]['timestamp'])

    return sorted_list


def get_degree_dict(nx_graph):
    degree_dict = defaultdict(lambda: 0)
    for node in nx_graph.nodes():
        degree_dict[node] = len(nx_graph.edges(node))
    return degree_dict


def read_walks(path, delimiter=','):
    walks = []
    with open(path, 'r') as f:
        for line in f:
            walks.append(line.strip().split(delimiter))

    return walks
