import numpy as np

from icmen.utils import dataset


def basic_scoring_function(x, y):
    return np.power(np.abs(x - y), 1) * (np.pi / 2 - np.arctan(np.maximum(x, y)))


def get_reference_node_scores_dict(prev_degree_dict, curr_degree_dict, scoring_function):
    return {
        node: scoring_function(prev_node_degree, curr_degree_dict[node])
        for node, prev_node_degree in prev_degree_dict.items()
    }


def get_reference_nodes_scores(previous_graph, current_graph):
    previous_degree = dataset.get_degree_dict(previous_graph)
    current_degree = dataset.get_degree_dict(current_graph)

    reference_nodes_scores = get_reference_node_scores_dict(
        previous_degree, current_degree, basic_scoring_function
    )

    return reference_nodes_scores


def get_reference_nodelist(scores_dict, reference_nodes_count):
    reference_nodelist = sorted(
        scores_dict.keys(), key=scores_dict.get
    )[:reference_nodes_count]

    reference_nodelist = [str(node) for node in reference_nodelist]

    return reference_nodelist
