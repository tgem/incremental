"""
Edge embeddings
"""


def hadamard_op(arg1, arg2):
    return arg1 * arg2


def edge_embedding(node_embedding, edges, op):
    edge_embs = []

    for n1, n2 in edges:
        n1_emb = node_embedding.get_vector(str(n1))
        n2_emb = node_embedding.get_vector(str(n2))

        edge_embs.append(op(n1_emb, n2_emb))

    return edge_embs
