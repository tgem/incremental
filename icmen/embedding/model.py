"""
Embedding data class definition
(In our case this class will represent a node embedding)
"""
import pickle as pkl

import gensim
import numpy as np


class Embedding:
    def __init__(self, size, node_emb_vectors=None):
        self._emb_dim = size
        self._node_emb_vectors = node_emb_vectors or {}

    def add_node(self, node, embedding_vector):
        assert len(embedding_vector) == self._emb_dim
        self._node_emb_vectors[node] = (np.array(embedding_vector)
                                        .astype('float32'))

    def get_vector(self, node):
        if node in self._node_emb_vectors.keys():
            return self._node_emb_vectors[node]

        return np.ones(self._emb_dim)

    def get_subset(self, nodes):
        subset = dict()
        for node in nodes:
            subset[node] = self.get_vector(node)
        return subset

    @classmethod
    def from_file(cls, filepath):
        with open(filepath, 'rb') as f:
            emb_dict = pkl.load(f)

        return Embedding(
            node_emb_vectors=emb_dict.get('vectors'),
            size=emb_dict.get('size')
        )

    @classmethod
    def from_gensim_w2v_file(cls, filepath):
        """Load embedding from given path
        We assume here that it is stored in Word2Vec format
        """
        w2v_emb = gensim.models.KeyedVectors.load_word2vec_format(filepath)
        return cls.from_w2v_format(w2v_embedding=w2v_emb)

    @classmethod
    def from_w2v_format(cls, w2v_embedding):
        """Convert Word2Vec format to our format"""
        node_emb_vecs = {}
        for idx, node in enumerate(w2v_embedding.index2word):
            node_emb_vecs[node] = w2v_embedding.vectors[idx]

        return Embedding(
            size=w2v_embedding.vector_size,
            node_emb_vectors=node_emb_vecs
        )

    def to_pickle(self, filepath):
        """Save current embedding to file"""
        with open(filepath, 'wb') as f:
            pkl.dump(
                obj={'vectors': self._node_emb_vectors,
                     'size': self._emb_dim},
                file=f
            )

    def to_numpy(self, nodelist):
        arr = []
        for node in nodelist:
            arr.append(self._node_emb_vectors.get(str(node)))
        return np.array(arr)

    def items(self):
        return self._node_emb_vectors.items()

    @property
    def nodes(self):
        return list(self._node_emb_vectors.keys())

    @property
    def shape(self):
        return len(self.nodes), self._emb_dim

    @property
    def emb_dim(self):
        return self._emb_dim

    def __repr__(self):
        return 'Embedding(shape={})'.format(self.shape)


class AbstractRandomWalkEmbedding:
    def __init__(self, w2v_params):
        self._model = gensim.models.Word2Vec(**w2v_params)

    def train_model(self, rw_seqs, epochs):
        self._model.train(
            rw_seqs,
            total_examples=len(rw_seqs),
            epochs=epochs
        )

    def to_keyed_model(self):
        return Embedding.from_w2v_format(self._model.wv)

    def train(self, *args):
        pass


class RandomWalkEmbedding(AbstractRandomWalkEmbedding):
    def __init__(self, w2v_params):
        super(RandomWalkEmbedding, self).__init__(w2v_params)

    def train(self, rw_seqs, epochs=5):
        self._model.build_vocab(rw_seqs)
        self.train_model(rw_seqs, epochs)
        return self.to_keyed_model()


class CalibratedRandomWalkEmbedding(AbstractRandomWalkEmbedding):
    def __init__(self, w2v_params, reference_nodelist):
        super(CalibratedRandomWalkEmbedding, self).__init__(w2v_params)
        self.reference_nodelist = reference_nodelist

    def init_vectors(self, prev_emb_dict):
        for node, vec in prev_emb_dict.items():
            self._model.wv.vectors[
                self._model.wv.vocab[node].index
            ] = vec

    def update_w2v_vocab(self):
        min_count = self._model.min_count
        model_vocab = list(self._model.wv.vocab.keys())
        to_update_list = [
            node for node in self.reference_nodelist
            if not node in model_vocab
        ]
        self._model.build_vocab(
            [to_update_list] * min_count,
            update=True
        )

    def train(self, rw_seqs, prev_emb, regression_model, epochs=5):
        self._model.build_vocab(rw_seqs)
        self.update_w2v_vocab()
        self.init_vectors(prev_emb.get_subset(self.reference_nodelist))
        self.train_model(rw_seqs, epochs)
        return self.calibrate(
            curr_embedding=Embedding.from_w2v_format(self._model.wv),
            reference_embedding=prev_emb,
            reference_nodelist=self.reference_nodelist,
            regression_model=regression_model,
        )

    @classmethod
    def calibrate(cls, curr_embedding, reference_embedding, reference_nodelist, regression_model):
        nodelist = curr_embedding.nodes
        emb_array = curr_embedding.to_numpy(nodelist)
        fit_emb_array = curr_embedding.to_numpy(reference_nodelist)
        ref_emb_array = reference_embedding.to_numpy(reference_nodelist)

        regression_model.fit(fit_emb_array, ref_emb_array)
        calibrated_emb_array = regression_model.predict(emb_array)

        return Embedding(
            size=calibrated_emb_array.shape[1],
            node_emb_vectors=dict(zip(nodelist, calibrated_emb_array))
        )


def get_node_vectors_from_gensim(model, nodelist):
    return {
        node: model.wv.get_vector(node)
        for node in nodelist
    }
