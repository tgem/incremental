from collections import defaultdict

import numpy as np

from icmen.embedding import model as emb_model
from icmen.fusion import estimators as es
from icmen.fusion import validation as val


class AbstractICMEN:
    def __init__(self, window_size, parameters):
        self._window_size = window_size
        self._parameters = parameters

    def predict(self, input_embeddings):
        pass

    @property
    def parameters(self):
        return self._parameters


class BasicICMEN(AbstractICMEN):
    def __init__(self, window_size, alpha):
        parameters = (alpha, 1 - alpha)
        super(BasicICMEN, self).__init__(window_size, parameters)

    def predict(self, input_embeddings):
        assert len(input_embeddings) == self._window_size
        return combine_embeddings(self._parameters, input_embeddings, combine_vectors_convex)


class ICMEN(AbstractICMEN):
    def __init__(self, window_size):
        parameters = np.zeros(window_size)
        super(ICMEN, self).__init__(window_size, parameters)

    def fit(self, embeddings, last_snapshot, prior_strategy):
        corr_pred = val.get_correct_edge_predictions(embeddings, last_snapshot)

        if prior_strategy == 'uniform':
            alpha = [1.0] * self._window_size
        elif prior_strategy == 'increase':
            alpha = list(range(1, self._window_size + 1))
        else:
            raise RuntimeError('Unknown prior strategy:', prior_strategy)

        # Get parameters using Dirichlet
        dirichlet = es.DirichletMultinomialModel(
            alpha=alpha,
            N=corr_pred,
            dim=self._window_size,
        )

        self._parameters = dirichlet.map()

        return self

    def predict(self, input_embeddings):
        assert len(input_embeddings) == self._window_size
        return combine_embeddings(self._parameters, input_embeddings, combine_vectors)


def norm(s):
    if not np.any(s):  # Contains only zeros
        return s
    return s / sum(s)


def combine_vectors(parameters, vectors):
    pv = [(p, v) for p, v in zip(parameters, vectors) if v is not None]

    if len(pv) == 0:
        raise RuntimeError("combine_vectors() called with no vectors!")

    parameters, vectors = zip(*pv)

    if len(vectors) == 1:
        return list(vectors[0])

    parameters = norm(np.array(parameters))
    vectors = np.array(vectors)

    weighed_vectors = (vectors.T * parameters).T
    combined_vector = np.sum(weighed_vectors, axis=0)

    return list(combined_vector)


def combine_vectors_convex(parameters, vectors):
    if len(vectors) == 1:
        return list(vectors[0])

    combined_vector = None
    for vector in vectors:
        if vector is not None:
            if combined_vector is None:
                combined_vector = vector
            else:
                combined_vector = combined_vector * parameters[0] + vector * parameters[1]

    return list(combined_vector)


def combine_embeddings(parameters, input_embeddings, combine_vec_fn):
    # Gather node vectors
    node_vectors = defaultdict(lambda: [None] * len(input_embeddings))
    for idx, emb in enumerate(input_embeddings):
        for node in emb.nodes:
            node_vectors[node][idx] = emb.get_vector(node)

    # Construct new embedding
    emb_predicted = emb_model.Embedding(size=input_embeddings[0].emb_dim)

    for node, vectors in node_vectors.items():
        emb_predicted.add_node(node, combine_vec_fn(parameters, vectors))

    return emb_predicted
