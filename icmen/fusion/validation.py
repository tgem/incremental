"""
Validation models for training and evaluation of ICMEN
"""
import warnings

import numpy as np
from sklearn import linear_model as sk_lm
from sklearn import metrics as sk_ms
from sklearn import exceptions as sk_exc

from icmen.embedding import edge_embeddings as ee
from icmen.utils import dataset as ds

# Do not show scikit warnings
warnings.filterwarnings("ignore", category=sk_exc.UndefinedMetricWarning)


class ValidationModel:
    def fit(self, x, y):
        pass

    def predict(self, x):
        pass

    def validate(self, x, y):
        y_pred = self.predict(x)
        return prediction_report(y_pred=y_pred, y_true=y)


class LinkPredictionValidationModel(ValidationModel):
    def __init__(self, embedding):
        self._emb = embedding
        self._log_reg = sk_lm.LogisticRegression(solver='liblinear')

    def fit(self, x, y):
        self._log_reg.fit(X=self._to_edge_emb(x), y=y.transpose()[0])

    def predict(self, x):
        return self._log_reg.predict(self._to_edge_emb(x))

    def _to_edge_emb(self, x):
        return ee.edge_embedding(self._emb, x, ee.hadamard_op)


def prediction_report(y_pred, y_true):
    metrics = sk_ms.precision_recall_fscore_support(y_true, y_pred)
    accuracy = sk_ms.accuracy_score(y_true, y_pred)
    cm = sk_ms.confusion_matrix(y_true, y_pred)

    accuracy_per_class = np.array([row[i] / np.sum(row)
                                   for i, row in enumerate(cm)])

    fpr, tpr, _ = sk_ms.roc_curve(y_pred, y_true)

    report = dict()
    report['auc'] = sk_ms.auc(fpr, tpr)
    report['precision'] = metrics[0]
    report['recall'] = metrics[1]
    report['f1-score'] = metrics[2]
    report['support'] = metrics[3]
    report['accuracy'] = accuracy_per_class
    report['avg_accuracy'] = accuracy

    return report


def make_validation_fn(vm_cls, graph_snapshot):
    dataset = ds.create_link_prediction_dataset_from_graph(
        graph=graph_snapshot,
        split_proportion=0.75
    )

    x_train, y_train = dataset[0], dataset[1]
    x_test, y_test = dataset[2], dataset[3]

    def val_fn(embedding):
        vm = vm_cls(embedding)
        vm.fit(x_train, y_train)
        report = vm.validate(x_test, y_test)
        return 1.0 - report['auc']

    return val_fn


def get_validation_errors(embeddings, snapshot):
    val_fn = make_validation_fn(
        vm_cls=LinkPredictionValidationModel,
        graph_snapshot=snapshot
    )

    val_errs = np.array([val_fn(emb) for emb in embeddings])

    return val_errs


def get_correct_edge_predictions(embeddings, snapshot):
    # Create dataset for learning logistic regression
    dataset = ds.create_link_prediction_dataset_from_graph(
        graph=snapshot,
        split_proportion=0.75
    )

    x_train, y_train = dataset[0], dataset[1]
    x_test, y_test = dataset[2], dataset[3]

    # Extract true edges
    true_edges = []

    for x, y in zip(x_train, y_train):
        if y[0] == 1:
            true_edges.append(x)

    for x, y in zip(x_test, y_test):
        if y[0] == 1:
            true_edges.append(x)

    # Prepare validation models
    vms = [LinkPredictionValidationModel(emb) for emb in embeddings]

    for vm in vms:
        vm.fit(x_train, y_train)

    # Count correct predictions
    correct_predictions = [0] * len(embeddings)

    preds = [vm.predict(true_edges) for vm in vms]
    # Zip together outcomes from different models for one given edge
    preds = list(zip(*preds))

    for pred in preds:
        # More than one embedding "predicted" the edge correctly
        if sum(list(pred)) > 1:
            idx = np.random.choice([i for i, p in enumerate(pred) if p == 1])
            correct_predictions[idx] += 1
        # Only one embedding "predicted" the edge correctly
        elif sum(list(pred)) == 1:
            idx = pred.index(1)
            correct_predictions[idx] += 1

    return correct_predictions
