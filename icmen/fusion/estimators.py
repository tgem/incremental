"""
Parameter estimators for ICEMO model
"""
import numpy as np


class ParameterEstimator:
    def __init__(self, nb_parameters):
        self._nb_parameters = nb_parameters
        self._state = {}

    def get_parameters(self, hyperparameters):
        pass


class DirichletEstimator(ParameterEstimator):
    def __init__(self, nb_parameters, prior):
        super().__init__(nb_parameters=nb_parameters)
        self._prior = prior

    def get_parameters(self, hyperparameters):
        dmm = DirichletMultinomialModel(alpha=self._prior,
                                        N=hyperparameters,
                                        dim=self._nb_parameters)
        yield tuple(dmm.map())


class DirichletMultinomialModel:
    def __init__(self, alpha, N, dim):
        self._alpha = np.array(alpha)  # Prior
        self._N = np.array(N)  # Outcomes
        self._dim = dim  # Dimensionality

    def map(self):
        N = sum(self._N)
        alpha0 = sum(self._alpha)
        _map = (self._N + self._alpha - 1) / (N + alpha0 - self._dim)
        return _map

    def mle(self):
        _mle = self._N / sum(self._N)
        return _mle
