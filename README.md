*ICMEN* Incremental Convex Meta Embedding for Nodes
---
Authors implementation of ICMEN paper

# Examples
We prepared a simple script to test ICMEN on your own:

- Training calibrated embeddings
```bash
PYTHONPATH=. python3 examples/train_calibrated_embeddings.py \
            --walks data/fb-forum/random_walks/n2v/RW01 \
                    data/fb-forum/random_walks/n2v/RW12 \
                    data/fb-forum/random_walks/n2v/RW23 \
                    data/fb-forum/random_walks/n2v/RW34 \
            --snapshots data/fb-forum/snapshots/G01 \
                        data/fb-forum/snapshots/G12 \
                        data/fb-forum/snapshots/G23 \
                        data/fb-forum/snapshots/G34 \
            --reference-nodes-ratio 0.7 \
            --output-path data/fb-forum/embeddings/n2v_calibrated \
            --dimension 128 \
            --workers 24 \
            --min-count 5 \
            --skip-gram 0 
```
- Basic ICMEN

```bash
PYTHONPATH=. python3 examples/run_basic_icmen.py \
            --embeddings data/fb-forum/embeddings/n2v_calibrated/F01 \
                         data/fb-forum/embeddings/n2v_calibrated/F12 \
                         data/fb-forum/embeddings/n2v_calibrated/F23 \
                         data/fb-forum/embeddings/n2v_calibrated/F34 \
            --alpha 0.2 \
            --test-graph data/fb-forum/test_graph.gpkl
```

Example output:
```bash
Loading data...
Running ICMEN...
Testing estimated embedding...
Basic ICMEN parameters (0.2, 0.8)
Combined embedding AUC on test graph: 77.79 %
```

- Generalized ICMEN

```bash
PYTHONPATH=. python3 examples/run_generalized_icmen.py \
            --embeddings data/fb-forum/embeddings/n2v_calibrated/F01 \
                         data/fb-forum/embeddings/n2v_calibrated/F12 \
                         data/fb-forum/embeddings/n2v_calibrated/F23 \
                         data/fb-forum/embeddings/n2v_calibrated/F34 \
            --last-snapshot data/fb-forum/last_snapshot.gpkl \
            --prior-strategy uniform \
            --test-graph data/fb-forum/test_graph.gpkl
```

Example output:
```bash
Loading data...
Running ICMEN...
Testing estimated embedding...
stimated ICMEN parameters: [0.1852 0.1892 0.2258 0.3998]
Combined embedding AUC on test graph: 77.77 %
```
